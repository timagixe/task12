import CartParser from './CartParser';

import {
    validCSV,
    invalidHeaderInCSV,
    invalidHeaderInCSVErrorObject,
    emptyCellInCSV,
    emptyCellInCSVErrorObject,
    wrongCellsNumberInCSV,
    wrongCellsNumberInCSVErrorObject,
    negativeNumberInCSV,
    negativeNumberInCSVErrorObject,
    csvLineToJSON,
    inValidCSV,
} from '../samples/validationConstants';

let parser;

beforeEach(() => {
    parser = new CartParser();
});

describe('CartParser - unit tests', () => {
    // Add your unit tests here.

    describe('validate', () => {
        it('should pass validation when CSV is OK', () => {
            expect(parser.validate(validCSV)).toHaveLength(0);
        });

        it('should not pass validation with wrong header name', () => {
            expect(parser.validate(invalidHeaderInCSV)).toContainEqual(
                invalidHeaderInCSVErrorObject
            );
        });

        it('should not pass validation with wrong cells number', () => {
            expect(parser.validate(wrongCellsNumberInCSV)).toContainEqual(
                wrongCellsNumberInCSVErrorObject
            );
        });

        it('should not pass validation with empty cell string', () => {
            expect(parser.validate(emptyCellInCSV)).toContainEqual(
                emptyCellInCSVErrorObject
            );
        });

        it('should not pass validation with negative cell', () => {
            expect(parser.validate(negativeNumberInCSV)).toContainEqual(
                negativeNumberInCSVErrorObject
            );
        });
    });

    describe('calcTotal', () => {
        it('should return correct total sum', () => {
            expect(parser.calcTotal([{ price: 10, quantity: 100 }])).toBe(1000);
        });
    });

    describe('createError', () => {
        it('should create valid error object with valid error properties', () => {
            const {
                column,
                message,
                row,
                type,
            } = invalidHeaderInCSVErrorObject;

            const generatedError = parser.createError(
                type,
                row,
                column,
                message
            );

            expect(generatedError).toEqual(invalidHeaderInCSVErrorObject);
        });
    });

    describe('parseLine', () => {
        it('should convert CSV lien to JSON', () => {
            parser.parseLine = jest.fn(() => ({
                name: 'Condimentum aliquet',
                price: 13.9,
                quantity: 1,
            }));

            expect(parser.parseLine('Condimentum aliquet,13.90,1')).toEqual(
                csvLineToJSON
            );
        });
    });

    describe('parse', () => {
        it('should throw error if CSV is not OK', () => {
            parser.readFile = jest.fn(() => inValidCSV);

            expect(() => parser.parse('dataFromInValidCSV')).toThrowError();
        });
    });
});

describe('CartParser - integration test', () => {
    // Add your integration test here.

    it('should return object with props like items typeof array and total typeof number ', () => {
        const csvToJSON = parser.parse('./samples/cart.csv');

        expect(Array.isArray(csvToJSON.items)).toBe(true);
        expect(typeof csvToJSON.total).toBe('number');
    });
});
