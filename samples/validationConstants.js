export const validCSV = `Product name,Price,Quantity
Mollis consequat,9.00,2
Tvoluptatem,10.32,1
Scelerisque lacinia,18.90,1
Consectetur adipiscing,28.72,10
Condimentum aliquet,13.90,1
`;

export const inValidCSV = `
Product, name,Pri,ce,Quantity
Mollis consequat,9.00,2
Tvolupta,

tem,10.32,1

S,cel
erisque lacinia,18.90,1
,Con
,sectetur adipiscing,28.72,10
Condimentum aliquet,13.90,1	
`;

export const invalidHeaderInCSV = `1Product name,Price,Quantity
Mollis consequat,9.00,2
Tvoluptatem,10.32,1
Scelerisque lacinia,18.90,1
Consectetur adipiscing,28.72,10
Condimentum aliquet,13.90,1
`;

export const invalidHeaderInCSVErrorObject = {
    column: 0,
    message:
        'Expected header to be named "Product name" but received 1Product name.',
    row: 0,
    type: 'header',
};

export const emptyCellInCSV = `Product name,Price,Quantity
Mollis consequat,9.00,2
Tvoluptatem,10.32,1
Scelerisque lacinia,18.90,1
Consectetur adipiscing,28.72,10
,13.90,1
`;

export const emptyCellInCSVErrorObject = {
    column: 0,
    message: 'Expected cell to be a nonempty string but received "".',
    row: 5,
    type: 'cell',
};

export const wrongCellsNumberInCSV = `Product name,Price,Quantity
Mollis consequat,9.00,2
Tvoluptatem,10.32,1
Scelerisque lacinia,18.90,1
Consectetur adipiscing
Condimentum aliquet,13.90,1
`;

export const wrongCellsNumberInCSVErrorObject = {
    column: -1,
    message: 'Expected row to have 3 cells but received 1.',
    row: 4,
    type: 'row',
};

export const negativeNumberInCSV = `Product name,Price,Quantity
Mollis consequat,-9.00,2
Tvoluptatem,10.32,1
Scelerisque lacinia,18.90,1
Consectetur adipiscing,28.72,10
Condimentum aliquet,13.90,1
`;

export const negativeNumberInCSVErrorObject = {
    column: 1,
    message: 'Expected cell to be a positive number but received "-9.00".',
    row: 1,
    type: 'cell',
};

export const csvLineToJSON = {
    name: 'Condimentum aliquet',
    price: 13.9,
    quantity: 1,
};
